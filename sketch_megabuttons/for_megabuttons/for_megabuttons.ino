#include <pitchToNote.h>
#include <pitchToFrequency.h>
#include <MIDIUSB.h>
#include <MIDIUSB_Defs.h>
#include <frequencyToNote.h>

const int SERIAL_FREQ = 9600;
const int FIRST_BUTTON= 22;
const int FIRST_LED = 34;
const int NUM_BUTTONS = 10;
const int FIRST_NOTE = 47;
const String LED_ON_COMMAND = "led_on";
const String LED_OFF_COMMAND = "led_off";
const unsigned long long BUTTON_DELAY = 50; // in msec

unsigned long long checkpoint = 0; 
int buttonstate[2][NUM_BUTTONS] = {{LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW}, {LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW}};
bool ledstate[NUM_BUTTONS] = {LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW};
int acc = 0, q = 0, st;

void setup(){
    Serial.begin(SERIAL_FREQ);
    Serial.setTimeout(1);
    for(int i = 0; i < NUM_BUTTONS; ++i){
        pinMode(FIRST_BUTTON + i, INPUT_PULLUP);
        pinMode(FIRST_LED + i, OUTPUT);
    }
}

void loop(){
    if(Serial.available()){
         String command = Serial.readString();
        Serial.println("Got " + command);
        if (command.startsWith(LED_ON_COMMAND)){
            String buttonNum_s = command.substring(LED_ON_COMMAND.length());
            buttonNum_s.trim();
            int buttonNum = buttonNum_s.toInt();
            st = buttonNum;
            q = HIGH;
            ledstate[st] = q;
        }
        if (command.startsWith(LED_OFF_COMMAND)){
            // Reading button pin to turn led off
            String buttonNum_s = command.substring(LED_OFF_COMMAND.length());
            buttonNum_s.trim();
            int buttonNum = buttonNum_s.toInt();
            st = buttonNum;
            q = LOW;
            ledstate[st] = q;
        }    


        /*
        st = Serial.readString().toInt();
        Serial.print("Got value:");
        Serial.println(st);
        if(acc == 0){
            Serial.print("Changing state to accept");
            Serial.println();
            acc = 1;
            if(st == 1){
                q = HIGH;
            }else if(st == 0){
                q = LOW;
            }
        } else{
            ledstate[st] = q;
            acc = 0;
            Serial.print("Applied ");
            Serial.print(q);
            Serial.print(" to led ");
            Serial.println(st);
        }
        */
    }

    while(!Serial.available()){
        for(int i = 0; i < NUM_BUTTONS; ++i){
            buttonstate[1][i] = digitalRead(FIRST_BUTTON + i);
            digitalWrite(FIRST_LED + i, ledstate[i]);
        }
        playNotes();
        for(int i = 0; i < NUM_BUTTONS; ++i){
            buttonstate[0][i] = buttonstate[1][i];
        }
    }   
}

//=======================================================

void playNotes(){
    for(int i = 0; i < NUM_BUTTONS ; ++i){
        if(buttonstate[0][i] == LOW && buttonstate[1][i] == HIGH)
            Serial.println(i);
            Serial.println("pressed")
        if(buttonstate[0][i] == HIGH && buttonstate[1][i] == LOW)
            Serial.println(i);
            Serial.println("released")
    }
}
