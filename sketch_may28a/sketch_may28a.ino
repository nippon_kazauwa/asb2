#include <pitchToNote.h>
#include <pitchToFrequency.h>
#include <MIDIUSB.h>
#include <MIDIUSB_Defs.h>
#include <frequencyToNote.h>

const int SERIAL_FREQ = 9600;
const int FIRST_BUTTON= 22;
const int FIRST_LED = 34;
const int NUM_BUTTONS = 10;
const int FIRST_NOTE = 47;
int buttonstate[2][NUM_BUTTONS] = {{LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW}, {LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW}};
bool ledstate[NUM_BUTTONS] = {LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW};
int acc = 0, q = 0, st;

void setup(){
    Serial.begin(SERIAL_FREQ);
    Serial.setTimeout(1);
    for(int i = 0; i < NUM_BUTTONS; ++i){
        pinMode(FIRST_BUTTON + i, INPUT_PULLUP);
        pinMode(FIRST_LED + i, OUTPUT);
    }
}

void loop(){
    //Serial api
    if(Serial.available()){
        st = Serial.readString().toInt();
        Serial.print("Got value:");
        Serial.println(st);
        if(acc == 0){
            Serial.print("State:");
            Serial.println();
            acc = 1;
            q = st;
        } else{
            if(st == 1) ledstate[q] = HIGH;
            if(st == 0) ledstate[q] = LOW;
            acc = 0;
            Serial.print("Applied ");
            Serial.print(st);
            Serial.print(" to led ");
            Serial.println(q);
        }
    }
    //
    while(!Serial.available()){
      for(int i = 0; i < NUM_BUTTONS; ++i){
          buttonstate[1][i] = digitalRead(FIRST_BUTTON + i);
          digitalWrite(FIRST_LED + i, ledstate[i]);
      }
      playNotes();
      for(int i = 0; i < NUM_BUTTONS; ++i){
          buttonstate[0][i] = buttonstate[1][i];
      }
    }   
}

//=======================================================

void noteOn(byte channel, byte pitch, byte velocity) {
    midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
    MidiUSB.sendMIDI(noteOn);
}
void noteOff(byte channel, byte pitch, byte velocity) {
    midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
    MidiUSB.sendMIDI(noteOff);
}
void playNotes(){
    for(int i = 0; i < NUM_BUTTONS ; ++i){
        if(buttonstate[0][i] == LOW && buttonstate[1][i] == HIGH)
            noteOff(0, FIRST_NOTE + i, 0);
        if(buttonstate[0][i] == HIGH && buttonstate[1][i] == LOW)
            noteOn(0, FIRST_NOTE + i, 127);
    }
    MidiUSB.flush();
}
void readNotes(){
}
